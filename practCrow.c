#include<stdio.h>
#include<string.h>
#define MAX 20

int validarNombres(char *,char *);
int validarEdad(int);

int main(){
	char nombre[MAX], nombreViejo[MAX], nombreJoven[MAX];
	char apellido[MAX], apellidoViejo[MAX],apellidoJoven[MAX];
	int edad,edadViejo,edadJoven,i,validador;
	float media=0;
	edadViejo=0;
	edadJoven=500;
	for(i=0;i<10;i++){
		validador=0;
		while(!validador){
			printf("Persona %d \n\n",i+1);
			printf("Ingrese el nombre: ");
			scanf("%s",nombre);
			printf("Ingrese el apellido: ");
			scanf("%s",apellido);
			printf("Ingrese la edad: ");
			scanf("%d",&edad);
			validador=(validarNombres(nombre,apellido) && validarEdad(edad));
			if(!validador){
				printf("intente de nuevo, ingeso mal algun dato\n\n");
			}
			else{
				media+=edad;
				if(edad>edadViejo){
					edadViejo=edad;
					strcpy(nombreViejo,nombre);
					strcpy(apellidoViejo,apellido);
				}
				if(edad<edadJoven){
					edadJoven=edad;
					strcpy(nombreJoven,nombre);
					strcpy(apellidoJoven,apellido);
				}
			}
		}
	}
	printf("Promedio de edad: %.2f\n",media/10);
	printf("Persona mas vieja: %s %s, edad: %d\n",nombreViejo,apellidoViejo,edadViejo);
	printf("Persona mas Joven: %s %s, edad: %d",nombreJoven,apellidoJoven,edadJoven);

}

int validarNombres(char * nombre,char * apellido){
	int i;
	if(nombre[0]>=65 && nombre[0]<=90 && apellido[0]>=65 && apellido[0]<=90){
		for(i=1;nombre[i]!=0;i++){
			if(nombre[i]<97 || nombre[i]>122){
				return 0;
			}
		}
		for(i=1;apellido[i]!=0;i++){
			if(apellido[i]<97 || apellido[i]>122){
				return 0;
			}
		}
		return 1;
		
	}
	return 0;
}

int validarEdad(int edad){
	return (edad>0 && edad<130);
}